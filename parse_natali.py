# -*- coding: utf-8 -*-
import time
import random
import urlparse
import urllib
import os
import re
import requests

from django.core.files import File
import Image
import tempfile

from catalog.models import Category, Product
from .utils import get_bs_soup, get_full_url


def get_category_from_main_page(soup_html):
    """
    Парсим категории с главной страницы
    :param soup_html: html страницы
    :return: словарь для создания категории
    """
    list_category = []
    for category in soup_html.find_all('a', {'class': 'm11'}):
        list_category.append({'caption': category.text, 'url': get_full_url('http://example.ru/', category.attrs['href'])})
    return list_category


def get_sub_category_list(soup_html):
    """
    Парсим подкатегории
    :param soup_html: html страницы
    :return: Словарь для создания подкатегории
    """
    # Парсинг подкатегорий
    list_category = []
    cats = soup_html.find_all('div', {'class': 'cat'})
    for cat in cats:
        a_href = cat.find('a')
        a_img = a_href.next
        list_category.append({
            'url': get_full_url('http://example.ru/', a_href.attrs['href']),
            'caption': a_img.attrs['title'],
            'image_url': a_img.attrs['src']
        })
    return list_category


def get_product_links_from_page(html_soup):
    return [get_full_url('http://example.ru/', item.next.attrs['href']) for item in html_soup.find_all('div', {'class': 'prod_img'})]


size_pattern = re.compile(r'\d+\s*-\s*\d+')
material_pattern = re.compile(r':([\s\S]+)')


def get_profuct_from_soup(html_soup):
    # Теги P с контентом менее 5 симколов удаляем из html_soup
    empty_tags = html_soup.findAll(lambda tag: tag.name == 'p' and len(tag.text) < 5)
    [empty_tag.extract() for empty_tag in empty_tags]

    price_2 = html_soup.find('div', {'class': 'price1_prod'})
    p_elems = price_2.parent.find_all('p')
    material_elem = material_pattern.findall(p_elems[0].text)
    size_elem = size_pattern.findall(p_elems[-1].text)

    product_dict = {
        'model_name': html_soup.find('div', {'class': 'prod_name'}).text,
        'cost': html_soup.find('div', {'class': 'price1_prod'}).text.split(u'руб.')[0].strip(),
        'image_url': html_soup.find('a', {'id': 'Zoomer'}).attrs['href'],
    }

    if len(size_elem) > 0:
        product_dict['size'] = size_elem[0].replace(' ', '')
    if len(material_elem) > 0:
        product_dict['material'] = material_elem[0].strip()

    return product_dict


def create_category():
    """
    Проверка категорий на сайте example, и создание категории если её нет в системе.
    :return:
    """
    soup_main_page = get_bs_soup('http://example.ru')
    for category in get_category_from_main_page(soup_main_page):
        category.update({'site_name': Category.CAT_SITE_NAT})
        parent, is_new = Category.objects.get_or_create(url=category['url'], defaults=category)
        # Новинки пропускаем
        if category['url'] == 'http://example.ru/cat.php?k=7':
            continue
        # Парсим и создаем подкатегории, между запросами делаем паузу
        soup_sub_category = get_bs_soup(category['url'])
        sub_category = get_sub_category_list(soup_sub_category)
        for sub_cat in sub_category:
            sub_cat.update({'parent': parent})
            Category.objects.get_or_create(url=sub_cat['url'], defaults=sub_cat)
        time.sleep(random.randint(1, 2))


def create_product():
    """
    Парсинг категории, получаем товары созадем товары в БД
    """
    categories = Category.objects.filter(parent__isnull=False)
    products = []
    i = 1

    for category in categories:
        page_links = []
        # Собрали все ссылки на карточки товаров
        html_soup = get_bs_soup(category.url)
        page_links += get_product_links_from_page(html_soup)
        pagination = html_soup.find_all('div', {'class': 'ukaz'})
        if len(pagination) == 2:
            # убираем гет параметры из урла.
            # urlparse.urlparse(category.url)._replace(query=None).geturl()
            for page_link in [urlparse.urlparse(category.url)._replace(query=None).geturl() + item.attrs['href'] for item in pagination[1].find_all('a')]:
                page_links += get_product_links_from_page(get_bs_soup(page_link))
                time.sleep(random.randint(1, 2))

        # Из ссылок карточек товаром получаем словари для создания товаров.
        for p_link in page_links:
            i += 1

            print i, p_link
            html_soup = get_bs_soup(p_link)
            product_dict = get_profuct_from_soup(html_soup)
            product_dict.update({
                'url': p_link,
                'category': category,
            })
            Product.objects.update_or_create(url=p_link, defaults=product_dict)
            time.sleep(random.randint(1, 2))


def create_product_image():
    """
    Загрузка фото с целевого сайта и сохранение в БД.
    :return:
    """
    products = Product.objects.all()
    for product in products:

        if not product.image:
            print product.id
            request = requests.get(product.image_url, stream=True)
            # Was the request OK?
            if request.status_code != requests.codes.ok:
                # Nope, error handling, skip file etc etc etc
                continue
            # Get the filename from the url, used for saving later
            file_name = product.image_url.split('/')[-1]
            # Create a temporary file
            lf = tempfile.NamedTemporaryFile()
            # Read the streamed image in sections
            for block in request.iter_content(1024 * 8):
                # If no more file then stop
                if not block:
                    break
                # Write image block to temporary file
                lf.write(block)
            # Save the temporary image to the model#
            # This saves the model so be sure that is it valid
            product.image.save(file_name, File(lf))
            product.save()

