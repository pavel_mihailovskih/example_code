@loginRequiredHttp
def page_market_screener(request, requestType):

    # Константы направление измения цены в форме
    SMSF_DIRECTION_UP = 'SMSF_DIRECTION_UP'
    SMSF_DIRECTION_DOWN = 'SMSF_DIRECTION_DOWN'
    SMSF_DIRECTION_CHOICES = (
        (SMSF_DIRECTION_UP, _('mls_485')),
        (SMSF_DIRECTION_DOWN, _('mls_486')),
    )

    PMS_TOP_10_MOVERS = 'top_10_movers'
    PMS_TOP_10_MARKETS = 'top_10_markets'
    PMS_A_B_ANALYSIS = 'a_b_analysis'
    PMS_TAB_MENU = (
        (PMS_TOP_10_MOVERS, u'TOP 10 movers'),
        (PMS_TOP_10_MARKETS, u'TOP 10 markets for buy/sell'),
        (PMS_A_B_ANALYSIS, u'А-В - analysis'),
    )

    def get_pms_tab_menu(menu_items, req_type):
        """
        пункты меню для переключения между формами.
        :return:
        """
        menu = []
        for url_pms_tab_menu, title_pms_tab_menu in menu_items:
            menu.append(
                {
                    'ipsmi_is_active': url_pms_tab_menu == req_type,
                    'ipsmi_url': django.core.urlresolvers.reverse('sili.views.page_market_screener', kwargs={'requestType': url_pms_tab_menu}),
                    'ipsmi_cap': title_pms_tab_menu,
                }
            )
        return menu


    # Форма ввода параметров отображения на странице сканер рынка
    class SiliMarketScreenerForm(django.forms.Form):
        smsf_period = django.forms.IntegerField(
            label=_('mls_481'),
            help_text=_('mls_482'),
            min_value=1,
            max_value=180,
            widget=django.forms.NumberInput(attrs={"style": "width: 40px;"})
        )
        smsf_direction = django.forms.CharField(
            label=_('mls_484'),
            max_length=48,
            widget=django.forms.RadioSelect(choices=SMSF_DIRECTION_CHOICES)
        )
        smsf_details = django.forms.BooleanField(
            label=u'Детализация',
            help_text=u'Если эта галочка установлена, то при нажатии на кнопку "Показать" ниже на странице '
                      u'отобразится таблица со всеми стаканами в системе для проверки правильности работы страницы. '
                      u'Галочка видна только у пользователя с ID=1 (admin@sili.ru) ',
            required=False,
        )

        # Переопределяем конструктор чтобы скрыть поле
        # для всех кроме админа.
        def __init__(self, request_for_hide_details, *args, **kwargs):
            hide_details = not slogic.isAdminUser(request_for_hide_details)
            super(SiliMarketScreenerForm, self).__init__(*args, **kwargs)
            if hide_details:
                del self.fields['smsf_details']

    class SiliMarketScreenerTop10MarketsForm(django.forms.Form):
        smstmf_start_location = django.forms.ModelChoiceField(
            label=_('mls_491'),
            queryset=models.KMarketplace.objects.all().order_by('km_caption'),
            help_text=_('mls_492')
        )
        smstmf_distance = django.forms.IntegerField(
            label=_('mls_493'),
            help_text=_('mls_494'),
            min_value=0
        )
        smstmf_product = django.forms.MultipleChoiceField(
            label=_('mls_495'),
            widget=django.forms.SelectMultiple,
            choices=models.KProduct.objects.all().values_list('id', 'kp_caption')
        )
        smstmf_output_deals = django.forms.CharField(
            label=_('mls_496'),
            max_length=48,
            widget=django.forms.RadioSelect(choices=slogic.SMSTM_OUTPUT_DEALS_CHOICES)
        )
        smsf_details = django.forms.BooleanField(
            label=u'Детализация',
            help_text=u'Если эта галочка установлена, то при нажатии на кнопку "Показать" ниже на странице '
                      u'отобразится таблица со всеми стаканами в системе для проверки правильности работы страницы. '
                      u'Галочка видна только у пользователя с ID=1 (admin@sili.ru) ',
            required=False,
        )

        def __init__(self, request, *args, **kwargs):
            # Показываем чекбокс Детализация только админам
            hide_smsf_details = slogic.isAdminUser(request)
            super(SiliMarketScreenerTop10MarketsForm, self).__init__(*args, **kwargs)
            self.fields['smstmf_product'].widget.attrs['class'] = 'selectpicker'
            self.fields['smstmf_product'].widget.attrs['multiple data-actions-box'] = "true"
            if slogic.isAdminUser(request) is False:
                del self.fields['smsf_details']


    class SiliMarketScreenerABAnalysisForm(django.forms.Form):
        smsaf_details = django.forms.BooleanField(
            label=u'Детализация',
            help_text=u'Если эта галочка установлена, то при нажатии на кнопку "Показать" ниже на странице '
                      u'отобразится таблица со всеми стаканами в системе для проверки правильности работы страницы. '
                      u'Галочка видна только у пользователя с ID=1 (admin@sili.ru) ',
            required=False,
        )

        def __init__(self, request, *args, **kwargs):
            # Показываем чекбокс Детализация только админам
            super(SiliMarketScreenerABAnalysisForm, self).__init__(*args, **kwargs)
            if slogic.isAdminUser(request) is False:
                del self.fields['smsaf_details']


    def getScreenerFormForRequestPost(request):
        return SiliMarketScreenerForm(hide_details, request.POST)

    # Данные для ищщеыекфз меню на странице
    pms_menu_tabs = get_pms_tab_menu(PMS_TAB_MENU, requestType)

    if requestType == PMS_TOP_10_MOVERS:
        # Если нажата кнопка "set notifications"
        # Обраюотка нажатий кнопка установки и сброса уведомления
        # Стоит выше чем обработка формы на случай если переход на старницу уведомления то чтобы
        # не нужно было ждать долгой подготовки таблиц т.к. при переходе это не потребуется.
        r = slogic.processSetOrClearNotifications(request, slogic.SSN_RETTYPE_SCREENER)
        if r:
            return r

        # Строки для отображения под формой - основные и тестовые (для проверки)
        rows = []
        trows = []
        is_trows = False

        # Нажата кнопка отпрваки заполненной формы или кнопка удаления сигнала - не важно
        # то надо инициализировать форму данными из страницы.
        form = SiliMarketScreenerForm(request)
        if request.POST:

            # Извлечение данных из формы
            # Если форма заполнена корректно, то не важно нажата кнопка отправки формы под формой
            # или не нажаата а нажата кнопка удаления уведомения, в обоих случаях - готовим таблицу для отображения
            # если же форма заполнена некорректно, то показаываем ошибки валидации только если нажата кнопка под формой.
            form = SiliMarketScreenerForm(request, request.POST)
            is_valid_form = form.is_valid()
            cleaned_data = None
            if is_valid_form:
                cleaned_data = form.cleaned_data

            # Сброс ошибок валидации если кнопка под формаой не была нажата а была нажата кнопка удаления уведомления
            elif 'pms_screner_params_form' not in request.POST:
                form = SiliMarketScreenerForm(request, request.POST)

            # Форма параметров валидна - не важно - отправили ее или кнопку удаления - то покажем таблицу
            if is_valid_form:

                # Текущий юзер
                cur_auth_user = slogic.getAuthorizedUser(request)

                # Направлние в форме выбрано вверх
                is_up_direction = form.cleaned_data['smsf_direction'] == SMSF_DIRECTION_UP

                # Заявки по стаканам
                ndays = form.cleaned_data['smsf_period']
                krequests_by_stakans = slogic.getKRequestsBuyMatchedForLastNdaysByStakans(ndays)

                # Тут готовим таблицу для проверки если включена галочка детализации
                is_trows = 'smsf_details' in form.cleaned_data and form.cleaned_data['smsf_details']
                for m, p, t in slogic.getAllStakansInSystemGenerator():

                    # Очередной стакан
                    sn = slogic.joinStakanNameFromModels(m, p, t)

                    # Урл на таблицу заявок по имени стака
                    req_url = slogic.getKRequestBuyMatchedUrl(sn, ndays)

                    # Отображаемое значение цены 1 и цены 2 и измеения
                    price1_display, price2_display, price_change_str, price_change = \
                        slogic.getMarketScreenerPrice1Price2Change(sn, krequests_by_stakans)

                    # Строка таблицы
                    trows.append({
                        'pmstr_stakan_name': sn,
                        'pmstr_price_1': price1_display,
                        'pmstr_price_2': price2_display,
                        'pmstr_price_change': price_change_str,
                        'pmstr_price_change_val': price_change,
                        'pmstr_stakan_name_requests_url': req_url,
                    })

                # Сортируем по проценту изменения
                # Но нулевые строки - всегда в конец
                trows_not_empty = filter(lambda r: r['pmstr_price_change_val'] is not None, trows)
                sort_field = '{s}pmstr_price_change_val'.format(s = "-" if is_up_direction else "")
                trows_not_empty = sortByFields(trows_not_empty, [sort_field])
                trows = trows_not_empty + filter(lambda r: r['pmstr_price_change_val'] is None, trows)

                # Тут заполняем не более 10 строк - положительные или отрицательные изменение цены
                # Для отображения юзеру.
                if is_up_direction:
                    trows_with_sign = filter(lambda r: r['pmstr_price_change_val'] > 0, trows_not_empty)[0:10]
                else:
                    trows_with_sign = filter(lambda r: r['pmstr_price_change_val'] < 0, trows_not_empty)[0:10]

                # Готовим поля для отображения в таблице у пользователя на основе тестовых строк.
                for r in trows_with_sign:

                    stakan_name = r['pmstr_stakan_name']
                    m, p, t = slogic.splitStakanNameIntoPartsModels(stakan_name)

                    # Отображаемое значение цены 1 и цены 2 и измеения
                    price1_display, price2_display, price_change_str, price_change = \
                        slogic.getMarketScreenerPrice1Price2Change(stakan_name, krequests_by_stakans)

                    rows.append({
                        'pmsr_city': m.km_caption,
                        'pmsr_product': p.kp_caption,
                        'pmsr_transport': t.kmot_caption,
                        'pmsr_order': slogic.getListAdminFieldSubmitOrderLink(stakan_name),
                        'pmsr_price_alert': slogic.getListAdminFieldAddPriceAlertButton(cur_auth_user, m, p, t),
                        'pmsr_notification': slogic.getListAdminFieldSetNotification(cur_auth_user, m, p, t),
                        'pmsr_bid': slogic.getBidForStakan(m, p, t, for_display=True),
                        'pmsr_ask': slogic.getAskForStakan(m, p, t, for_display=True),
                        'pmsr_fmp': price1_display,
                        'pmsr_lmp': slogic.getLastMatchedPriceForStakan(m, p, t, for_display=True),
                        'pmsr_change': price_change_str,
                    })

        return render(request, 'page_market_screener.html', {
            'pse_form': form,
            'pms_rows': rows,
            'pms_test_rows': trows,
            'pms_is_test_rows': is_trows,
            'pms_menu_tabs': pms_menu_tabs,
        })

    elif requestType == PMS_TOP_10_MARKETS:
        # Выводим список ТОП заявок в пределах расстояния от заданного города, а также по типу продажа/покупка
        # Строки для отображения под формой - основные и тестовые (для проверки)
        rows = []
        trows = []
        # Нажата кнопка отпрваки заполненной формы или кнопка удаления сигнала - не важно
        # то надо инициализировать форму данными из страницы.
        if request.method == 'POST':
            form = SiliMarketScreenerTop10MarketsForm(request, request.POST)
        else:
            form = SiliMarketScreenerTop10MarketsForm(request)
        is_valid_form = form.is_valid()
        if is_valid_form:
            import geopy.distance
            cleaned_data = form.cleaned_data
            cur_auth_user = slogic.getAuthorizedUser(request)
            # По полученным данным из форму получаем список с данными для вывода в таблице
            rows = slogic.getStakanAndKrequestForTop10Markets(cleaned_data["smstmf_start_location"],
                                                              cleaned_data["smstmf_distance"],
                                                              cleaned_data["smstmf_product"],
                                                              cleaned_data["smstmf_output_deals"],
                                                              cur_auth_user)

             # Тут готовим таблицу для проверки если включена галочка детализации
            is_trows = 'smsf_details' in form.cleaned_data and form.cleaned_data['smsf_details']

            if is_trows:
                trows = []
                base_url = django.core.urlresolvers.reverse('admin:sili_krequest_changelist')
                for trow in slogic.getStakanAndKrequestForTestTop10MArkets():
                    trows.append({
                        'pmstm_stakan': trow['sn'],
                        'pmstm_sell': slogic.formatPriceByCurrency(u'{0:.2f}'.format(trow['sell']), "EUR"),
                        'pmstm_buy': slogic.formatPriceByCurrency(u'{0:.2f}'.format(trow['buy']), "EUR"),
                        'pmstm_difference': slogic.formatPriceByCurrency(u'{0:.2f}'.format(trow['difference']), "EUR"),
                        'pmstm_link_filter_krequest': slogic.getKRequestFilteredListingLink(base_url,
                                                                                            {'stakan': trow['sn'],
                                                                                             'kr_status__exact': models.KRequest.KR_STATUS_PUB}),
                    })

        ctx = {
            'pse_form': form,
            'pms_rows': rows,
            'pms_menu_tabs': pms_menu_tabs,
            'pms_trows': trows,
        }
        return render(request, 'page_market_screener_top_10_markets.html', ctx)
    elif requestType == PMS_A_B_ANALYSIS:
        # Строки для отображения под формой - основные и тестовые (для проверки)
        rows = []
        trows = []
        active_page = 1
        num_pages = None
        page_range = []
        query_param = None
        sort_param = {
            'spread': None,
            'distance': None,
        }
        sort_list = []
        # Нажата кнопка отпрваки заполненной формы или кнопка удаления сигнала - не важно
        # то надо инициализировать форму данными из страницы.
        if request.method == 'POST':
            form = SiliMarketScreenerABAnalysisForm(request, request.POST)
        # Если в строке запроса есть хоть один параметр относящийся к выводу данных в таблице,
        # то инициализируем форму данными из GET запроса
        elif any(x in ['smsaf_details', 's_s', 's_d', 'p', 'stakans'] for x in request.GET):
            form = SiliMarketScreenerABAnalysisForm(request, request.GET)
        else:
            form = SiliMarketScreenerABAnalysisForm(request)
        is_valid_form = form.is_valid()
        num_page = int(request.GET.get('p', 1))
        if is_valid_form or any(x in ['smsaf_details', 's_s', 's_d', 'p', 'stakans'] for x in request.GET):
            query_param = urllib.urlencode(form.cleaned_data)

            # Проверяем параметры сортировки и добавляем в строку для формирования ссылок на странице
            # А так же в шаблоне проверяем нужно ли активировать блок с управлением сортировкой.
            sort_spread = request.GET.get('s_s')
            if sort_spread and sort_spread != '':
                query_param += '&s_s={}'.format(sort_spread)
                sort_list.append(sort_spread)
            if sort_spread == '-spread':
                sort_param['spread'] = 'spread'
            elif sort_spread == 'spread':
                sort_param['spread'] = '-spread'
            sort_distance = request.GET.get('s_d')
            if sort_distance and sort_distance != '':
                query_param += '&s_d={}'.format(sort_distance)
                sort_list.append(sort_distance)
            if sort_distance == '-distance':
                sort_param['distance'] = 'distance'
            elif sort_distance == 'distance':
                sort_param['distance'] = '-distance'

            # Формирование таблицы с данными A-B analysis
            paginated_rows = Paginator(
                object_list=slogic.getKRequestsForABAnalysis(auth_user=slogic.getAuthorizedUser(request), sort_param=sort_list, stakans=request.GET.get('stakans')),
                per_page=10
            )
            try:
                rows = paginated_rows.page(num_page)
                active_page = num_page
            except PageNotAnInteger:
                rows = paginated_rows.page(1)
                active_page = 1
            except EmptyPage:
                rows = paginated_rows.page(1)
                active_page = 1

            num_pages = paginated_rows.num_pages
            page_range = paginated_rows.page_range

            # Тут готовим таблицу для проверки если включена галочка детализации
            is_trows = 'smsaf_details' in form.cleaned_data and form.cleaned_data['smsaf_details']
            trows = slogic.getKRequestsForABAnalysisTest() if is_trows else []

        ctx = {
            'pmsa_menu_tabs': pms_menu_tabs,
            'pmsa_form': form,
            'pmsa_rows': rows,
            'pmsa_trows': trows,
            'page_range': page_range,
            'num_pages': num_pages,
            'num_page': num_page,
            'query_param': query_param,
            'sort_param': sort_param,
            'active_page': active_page,
        }
        return render(request, 'page_market_screener_a_b_analysis.html', ctx)
    else:
        raise RuntimeError(u'В адресе указан не верный тип страницы. Сейчасс указан %s' % requestType)


# Страница "Тест расстояний" у админа
@loginRequiredHttp
@roleRequiredHttp(models.KUser.KU_ROLE_ADMIN)
@annoying.decorators.render_to('page_stest_distance.html')
def page_stest_distance(request):

    # Форма выбора города
    mark_qs = models.KMarketplace.objects.all().order_by('km_caption')
    class SiliTestDistanceForm(django.forms.Form):
        stdf_marketplace = forms.ModelChoiceField(
            label=u'Город',
            queryset=mark_qs
        )

    selected_city = mark_qs.first()
    form = SiliTestDistanceForm(initial={'stdf_marketplace': selected_city.id})

    if request.POST:

        form = SiliTestDistanceForm(request.POST)
        if form.is_valid():

            selected_city = form.cleaned_data['stdf_marketplace']

            txt = u"Отправка произведена успешно."
            django.contrib.messages.add_message(request, django.contrib.messages.INFO, txt)

    import geopy.distance

    cities = []
    for m in mark_qs:
        city_1_coord = (selected_city.km_shirota, selected_city.km_dolgota)
        city_2_coord = (m.km_shirota, m.km_dolgota)
        cities.append({
            'pstdc_city1': selected_city.km_caption,
            'pstdc_city2': m.km_caption,
            'pstdc_coord1': selected_city.km_shirota + u' - ' + selected_city.km_dolgota,
            'pstdc_coord2': m.km_shirota + u' - ' + m.km_dolgota,
            'pstdc_dist_m': int(round(geopy.distance.vincenty(city_1_coord, city_2_coord).miles)),
            'pstdc_dist_k': int(round(geopy.distance.vincenty(city_1_coord, city_2_coord).km)),
        })

    return {
        'pstd_form': form,
        'pstd_cities': cities,
    }


# Форма для редактирвоания курса валют
class ExchangeRatesForm(forms.Form):
    usd_eur = forms.FloatField(label=u'USD/EUR', help_text=u'Введите актуальный курс доллара к евро, например 0,9323')
    gbr_eur = forms.FloatField(label=u'GBR/EUR',
                               help_text=u'Введите актуальный курс британского фунта к евро, например 1,1692')


@loginRequiredHttp
@roleRequiredHttp(models.KUser.KU_ROLE_ADMIN)
@annoying.decorators.render_to('page_exchange_rates.html')
def page_exchange_rates(request):
    """
    Сохраняем курсы валют и выводим таблицу для проверки вычисления цены по курсу
    :param request:
    :return:
    """
    usd_eur = models.exchange_rate.usd_eur
    gbr_eur = models.exchange_rate.gbr_eur
    form_initial = {'usd_eur': usd_eur, 'gbr_eur': gbr_eur}

    form = ExchangeRatesForm(request.POST or None, initial=form_initial)
    if request.method == 'POST' and form.is_valid():
        cleaned_data = form.cleaned_data
        for key, value in cleaned_data.items():
            dbsettings.loading.set_setting_value('sili.models', '', key, value)

    excange_rates_table = []
    krequest_qs = slogic.getKRequestByStakanName()
    rates = {
        "usd_eur": decimal.Decimal(str(models.exchange_rate.usd_eur)),
        "gbr_eur": decimal.Decimal(str(models.exchange_rate.gbr_eur)),
    }
    for krequest in krequest_qs:
        currency = krequest.kr_marketplace_m.km_currency
        orig_price = krequest.kr_price
        price_by_currency = slogic.getPriceDictByCurrency(orig_price, currency, rates)
        price_EUR = price_by_currency['EUR']/price_by_currency['USD']
        price_GBR = price_by_currency['GBR']/price_by_currency['EUR']

        excange_rates_table.append({
            'stakan_cap': u'%s--%s--%s' % (krequest.kr_marketplace_m.km_caption, krequest.kr_product_m.kp_caption, krequest.kr_mode_of_transort_m.kmot_caption),
            'price_usd': price_by_currency['USD'].quantize(slogic.TWOPLACES),
            'price_gbr': price_by_currency['GBR'].quantize(slogic.TWOPLACES),
            'price_eur': price_by_currency['EUR'].quantize(slogic.TWOPLACES),
            'res_usd_eur': decimal.Decimal(str(price_EUR)).quantize(slogic.FOURPLACES),
            'res_gbr_eur': decimal.Decimal(str(price_GBR)).quantize(slogic.FOURPLACES),
        })

    return {
        'exchange_rate_form': form,
        'excange_rates_table': excange_rates_table,
    }


@csrf_exempt
@annoying.decorators.ajax_request
def ajax_get_product_packing(request):
    """
    Возвращаем все упаковки для данного вида товара.
    :param request:
    :return:
    """
    product, = backend_ajax.extractAngularPostData(request, 'product')
    return {'packing': slogic.getPackagingByProduct(product)}


@csrf_exempt
@annoying.decorators.ajax_request
def ajax_widget_get_stakan_presentation_data(request):
    """
    Данные для заполнения формы пожачи заявки.
    :param request:
    :return:
    """
    stakan_name, = backend_ajax.extractAngularPostData(request, 'pmr_stakan_name')

    if stakan_name is None:
        stakan_pres_data = slogic.stakanEmptyPresentationData()
    else:
        # Если упаковка не задана, то удалем из стакана, т.к. она не учитывается при формировании данных.
        if u'@--' in stakan_name:
            stakan_name = stakan_name.replace(u'@--', u'')

        # Получаем таблицы для запрошенного стакана
        tbl1, tbl2, stakan_pres_data = getStakanPresentationDataFromStakanTables(stakan_name)
        stakan_pres_data = stakan_pres_data[stakan_name]

        # В виджете отправляем текущего пользователя.
        # Если пользователь анонимный то заявки не подсветятся, если авторизован, то будут подсвечены.
        slogic.markDepthOfMarketWithUserRequests(
            stakan_name,
            stakan_pres_data['pres_depth_of_market'],
            None if request.user.is_anonymous() else request.user,
        )

    return {
        'agspd_stakan_pres_data': stakan_pres_data,
        'agspd_stakan_limits': slogic.getStakanLimitsByStakanName(stakan_name),
    }


